var gulp = require('gulp'),
    jade = require('gulp-jade'),
    less = require('gulp-less'),
    autoprefix = require('gulp-autoprefixer'),
    minifyCSS = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    path = require('path');
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    plumber = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    critical = require('critical'),
    cp = require('child_process');
var Promise = require('es6-promise').Promise;
var jekyll = process.platform === "win32" ? "jekyll.bat" : "jekyll";
// Where do you store your Sass files?
var lessDir = 'assets/css';





gulp.task('css', function() {
    return gulp.src(lessDir + '/*.less')
    .pipe(plumber())
    .pipe(less())
    .pipe(autoprefix('last 10 version'))
    .pipe(gulp.dest('_site/assets/css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('assets/css'))
    .pipe(browserSync.reload({stream:true}))
    .pipe(notify({ message: 'Styles task complete' }));
});

gulp.task('js', function() {
  return gulp.src('assets/js/scripts.js')
    .pipe(plumber())
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'))
    .pipe(gulp.dest('_site/assets/js'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('clean', function() {
    return gulp.src(['css', 'js'], {read: false})
        .pipe(clean());
});

gulp.task('critical-css', function() {
    critical.generate({
        // Your base directory
        base: '_site/',
        // HTML source file
        src: 'index.html',
        // CSS output file
        dest: 'assets/css/critical.min.css',
        // Minify critical-path CSS
        minify: true
    });
});

// run this task by typing in gulp jade in CLI
gulp.task('jade', function() {
    return gulp.src('_jadefiles/_includes/*.jade')
        .pipe(jade()) // pip to jade plugin
        .pipe(gulp.dest('_includes')); // tell gulp our output folder
});
gulp.task('jade2', function() {
    return gulp.src('_jadefiles/_layouts/*.jade')
        .pipe(jade()) // pip to jade plugin
        .pipe(gulp.dest('_layouts')) // tell gulp our output folder
        .pipe(notify({ message: 'Jade task complete' }));

});
/**
 * Build the Jekyll Site
 */
gulp.task('jekyll-build', function (done) {
    browserSync.notify('Building Jekyll');
    return cp.spawn(jekyll, ['build'], {stdio: 'inherit'})
        .on('close', done);
});

/**
 * Rebuild Jekyll & do page reload
 */
gulp.task('jekyll-rebuild', ['jekyll-build'], function () {
    browserSync.reload();
});


gulp.task('watch', function() {
  // Watch .less files
  gulp.watch('assets/css/**/*.less', ['css']);
  gulp.watch('assets/css/*.less', ['css']);
  // Watch .js files
  gulp.watch('assets/js/**/*.js', ['js']);
  // Watch .jade files
  gulp.watch('_jadefiles/_includes/*.jade', ['jade']);
  gulp.watch('_jadefiles/_layouts/**/*.jade', ['jade2']);
  // Watch .html files and posts
  gulp.watch(['index.html', '_includes/*.html', '_layouts/*.html', '*.md', '_posts/*'], ['jekyll-rebuild']);
});
/**
 * Wait for jekyll-build, then launch the Server
 */
gulp.task('browser-sync', ['jekyll-build','css', 'jade'], function() {
    browserSync({
        server: {
            baseDir: '_site'
        },
        host: "localhost",
        files:['_site/*.html','_site/assets/**/main.css'],
        files:['_site/*.html','_site/assets/**/AdminLTE.css'],
        scriptPath: function(path,port,options) {
          return options.get('absolute');
        }
    });
});
gulp.task('default', ['clean'], function() {
    gulp.start('css', 'jade', 'jade2', 'watch', 'browser-sync');
});
